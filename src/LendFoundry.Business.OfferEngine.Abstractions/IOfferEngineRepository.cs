﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.OfferEngine.Persistence
{
    public interface IOfferEngineRepository : IRepository<IApplicationOffer>
    {

        Task<IApplicationOffer> GetApplicationOffers(string applicationNumber);

        Task<IApplicationOffer> AddDeal(string applicationNumber, IDealOffer request);

        Task<IApplicationOffer> AddLenderDeal(string applicationNumber, List<IDealOffer> request);

        Task<IApplicationOffer> AddUpdateOffer(string applicationNumber, IApplicationOffer offers);
        Task<IDealOffer> GetDeal(string applicationNumber);
        Task<List<IDealOffer>> GetLenderDeals(string applicationNumber);
        Task<bool> DeleteOffers(string applicationNumber);
    }
}