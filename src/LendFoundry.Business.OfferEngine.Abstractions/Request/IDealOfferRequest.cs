﻿using System.Collections.Generic;

namespace LendFoundry.Business.OfferEngine
{
    public interface IDealOfferRequest
    {
       List<IDealOffer> dealOffers { get; set; }
        string Source { get; set; }
    }
}