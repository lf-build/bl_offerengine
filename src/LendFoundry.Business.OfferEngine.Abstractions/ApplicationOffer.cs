﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.OfferEngine
{
    public class ApplicationOffer : Aggregate, IApplicationOffer
    {
        public ApplicationOffer()
        {
        }

        public string EntityType { get; set; }
        public string EntityId { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBusinessLoanOffer, BusinessLoanOffer>))]
        public List<IBusinessLoanOffer> Offers { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDealOffer, DealOffer>))]
        public IDealOffer DealOffer { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IDealOffer, DealOffer>))]
        public List<IDealOffer> LenderDeals { get; set; }
        public double MonthlyRevenue { get; set; }
        public double AverageDailyBalance { get; set; }
        public string Source { get; set; }
        public string CreatedBy { get; set; }
        public TimeBucket CreatedOn { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IDealOffer, DealOffer>))]
        public List<IDealOffer> DealHistory { get; set; }

        public string Funder { get; set; }
        public string ProductId { get; set; }
        public string Program { get; set; }
        public double SubmittedDate { get; set; }
        public double ApprovedDateTime { get; set; }
    }
}