﻿using LendFoundry.Foundation.Client;

namespace LendFoundry.Business.OfferEngine
{
    public interface IOfferEngineConfiguration:IDependencyConfiguration    {
      
        string DurationType { get; set; }
        double OriginatingFeeMinLoanAmount { get; set; }
        double FixedOriginatingFee { get; set; }
        double OriginatingFeePer { get; set; }
        double ACHFee { get; set; }
        string DefaultSource { get; set; }
        string ConnectionString { get; set; }
    }
}
