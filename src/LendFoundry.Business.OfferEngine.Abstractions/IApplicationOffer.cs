﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Business.OfferEngine
{
    public interface IApplicationOffer : IAggregate
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        List<IBusinessLoanOffer> Offers { get; set; }
        IDealOffer DealOffer { get; set; }
        List<IDealOffer> LenderDeals { get; set; }
        double MonthlyRevenue { get; set; }
        double AverageDailyBalance { get; set; }
        string Source { get; set; }
        string CreatedBy { get; set; }
        TimeBucket CreatedOn { get; set; }
        List<IDealOffer> DealHistory { get; set; }
       
        string Funder { get; set; }
        string ProductId { get; set; }
        double SubmittedDate { get; set; }
        double ApprovedDateTime { get; set; }
    }
}