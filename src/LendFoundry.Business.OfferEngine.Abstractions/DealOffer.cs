﻿using LendFoundry.Foundation.Date;

namespace LendFoundry.Business.OfferEngine
{
    public class DealOffer : IDealOffer
    {
        public double LoanAmount { get; set; }
        public double RepaymentAmount { get; set; }
        public double SellRate { get; set; }
        public double CommissionRate { get; set; }
        public double CommissionAmount { get; set; }
        public string DurationType { get; set; }
        public decimal TermPayment { get; set; }
        public decimal AmountFunded { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public string RoutingNumber { get; set; }
        public int Term { get; set; }
        //new added fields
        public double Comp { get; set; }
        public double MaxGross { get; set; }
        public double ProcessingFees { get; set; }
        public string Comments { get; set; }
        public string Program { get; set; }
        //new added fields
        public double ApprovedAmount { get; set; }
        public string TypeOfPayment { get; set; }
        public double PaymentAmount { get; set; }
        public int NumberOfPayment { get; set; }
        public double OriginatingFeeAmount { get; set; } //psf
        public double ACHFee { get; set; }
        public double BuyRate { get; set; }
        public double LenderReturn { get; set; }
        public double NetFundingRate { get; set; }
        public double AfterDefaultAssumption { get; set; }
        public int AverageLife { get; set; }
        public double IRR { get; set; }
        public string Grade { get; set; }       
        public string CreatedBy { get; set; }
        public TimeBucket CreatedOn { get; set; }
        public string OfferId { get; set; }
        public string LenderId { get; set; }
        public double ExpectedYield { get; set; }
        public string PaymentDay { get; set; }
         
    }
}