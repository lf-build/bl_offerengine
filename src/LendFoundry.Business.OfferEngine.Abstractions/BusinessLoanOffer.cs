﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Business.OfferEngine
{
    public class BusinessLoanOffer : IBusinessLoanOffer
    {
        public string OfferId { get; set; }
        public double MinAmount { get; set; }
        public double MaxAmount { get; set; }
        public double MinFactor { get; set; }
        public double MaxFactor { get; set; }
        public int MinDuration { get; set; }
        public int MaxDuration { get; set; }
        public string DurationType { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IOfferFee, OfferFee>))]
        public IList<IOfferFee> OfferFees { get; set; }
        //new added
        public double Comp { get; set; }
        public double MaxGross { get; set; }
        public double PSF { get; set; }
        public double OriginatingFeeAmount { get; set; }
        public double MinDailyPayment { get; set; }
        public double MaxDailyPayment { get; set; }
        public string Grade { get; set; }
        public bool IsManual { get; set; }
        public double LoanAmountCap { get; set; }
    }
}