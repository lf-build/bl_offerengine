﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.OfferEngine
{
    public interface IOfferFee
    {
        double FeeAmount { get; set; }
        OfferFeeType FeeType { get; set; }
        double FeePercentage { get; set; }
        bool IsIncludedInLoanAmount { get; set; }
    }
}
