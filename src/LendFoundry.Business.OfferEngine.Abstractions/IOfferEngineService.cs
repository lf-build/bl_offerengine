﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Business.OfferEngine
{
    public interface IOfferEngineService
    {
        Task<IApplicationOffer> SaveOffer(string entityType, string entityId,IApplicationOffer offers);
        Task<IApplicationOffer> AddDeal(string entityType, string entityId, IDealOfferRequest request);
        Task<IApplicationOffer> AddLenderDeal(string entityType, string entityId, IDealOfferRequest request);
        Task<IApplicationOffer> GetApplicationOffers(string entityType, string entityId);
        Task<IDealOffer> GetDeal(string entityType, string entityId);
        Task<List<IDealOffer>> GetLenderDeal(string entityType, string entityId);
        Task<bool> DeleteOffersAndDeal(string entityType, string entityId);
    }
}