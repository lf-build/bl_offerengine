﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Business.OfferEngine
{
    public class OfferEngineConfiguration : IOfferEngineConfiguration,IDependencyConfiguration
    {

        public string DurationType { get; set; }
        public double OriginatingFeeMinLoanAmount { get; set; }
        public double FixedOriginatingFee { get; set; }
        public double OriginatingFeePer { get; set; }
        public double ACHFee { get; set; }
        public string DefaultSource { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
