﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;

namespace LendFoundry.Business.OfferEngine
{
    public class ConcreteJsonConverter<T> : JsonConverter
    {
        #region Public Methods

        public override bool CanConvert(Type objectType) => true;

        public override object ReadJson(JsonReader reader,
         Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize<T>(reader);
        }

        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }

        #endregion Public Methods
    }

    public class ConcreteListJsonConverter<T, I> : JsonConverter
       where T : IList
    {
        #region Public Methods

        public override bool CanConvert(Type objectType) => true;

        public override object ReadJson(JsonReader reader,
         Type objectType, object existingValue, JsonSerializer serializer)
        {
            var result = serializer.Deserialize<T>(reader);
            if (result == null)
            {
                return result;
            }
            return result.Cast<I>().ToList();
        }

        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }

        #endregion Public Methods
    }
}
