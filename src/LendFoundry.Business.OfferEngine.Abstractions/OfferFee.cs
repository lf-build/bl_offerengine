﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.OfferEngine
{
    public class OfferFee : IOfferFee
    {
        public double FeeAmount { get; set; }
        public OfferFeeType FeeType { get; set; }
        public double FeePercentage { get; set; }
        public bool IsIncludedInLoanAmount { get; set; }
    }
}
