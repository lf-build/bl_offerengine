﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.OfferEngine
{
    public enum OfferFeeType
    {
        OneTime = 1,
        Recurring = 2,
        PercentageOfLoanAmount = 3
    }
}
