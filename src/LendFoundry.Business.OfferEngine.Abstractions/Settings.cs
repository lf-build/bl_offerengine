﻿using System;

namespace LendFoundry.Business.OfferEngine
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "business-offerengine";
    }
}