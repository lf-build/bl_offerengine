﻿using LendFoundry.Business.OfferEngine.Persistence;
using LendFoundry.Business.Application;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.DataAttributes;
using LendFoundry.ProductRule;
using System.Linq;

namespace LendFoundry.Business.OfferEngine
{
    public class OfferEngineService : IOfferEngineService
    {
        #region Constructor

        public OfferEngineService
      (

           IOfferEngineRepository offerEngineRepository,
           IGeneratorService offerEngineNumberGenerator,
           ILogger logger,
           IEventHubClient eventHubClient,
           OfferEngineConfiguration offerEngineConfiguration,
          ITenantTime tenantTime,
          ILookupService lookupService,
          ITokenReader tokenReader,
          ITokenHandler tokenHandler,
           IApplicationService businessApplicationService,
           IDataAttributesEngine dataAttributesService,
           IProductRuleService productRuleService
      )
        {         

            if (lookupService == null)
                throw new ArgumentNullException(nameof(lookupService));

            if (tokenReader == null)
                throw new ArgumentNullException(nameof(tokenReader));

            if (tokenHandler == null)
                throw new ArgumentNullException(nameof(tokenHandler));

            OfferEngineRepository = offerEngineRepository;
            OfferEngineNumberGenerator = offerEngineNumberGenerator;
            EventHubClient = eventHubClient;
            CommandExecutor = new CommandExecutor(logger);
            if (offerEngineConfiguration == null)
                throw new ArgumentNullException(nameof(offerEngineConfiguration));
            OfferEngineConfigurations = offerEngineConfiguration;
            TenantTime = tenantTime;
            LookupService = lookupService;
            TokenReader = tokenReader;
            TokenHandler = tokenHandler;
            Logger = logger;
            BusinessApplicationService = businessApplicationService;
            DataAttributesService = dataAttributesService;
            ProductRuleService = productRuleService;
        }

        #endregion Constructor

        #region Private Properties

        private IOfferEngineRepository OfferEngineRepository { get; }
        private IGeneratorService OfferEngineNumberGenerator { get; }
        private IEventHubClient EventHubClient { get; }
        private ITenantTime TenantTime { get; }
        private CommandExecutor CommandExecutor { get; }
        private OfferEngineConfiguration OfferEngineConfigurations { get; }
        private ILookupService LookupService { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }    
        private ILogger Logger { get; }
        private IApplicationService BusinessApplicationService { get; }
        private IDataAttributesEngine DataAttributesService { get; }
        private IProductRuleService ProductRuleService { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IApplicationOffer> GetApplicationOffers(string entityType, string entityId)
        {
            if (entityId == null)
                throw new ArgumentNullException(nameof(entityId));
            try
            {
                return await OfferEngineRepository.GetApplicationOffers(entityId);
            }catch(Exception exception)
            {
                Logger.Error("Error While Processing GetApplicationOffers Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: OfferEngine" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<IApplicationOffer> AddDeal(string entityType, string entityId, IDealOfferRequest request)
        {
            if (entityId == null)
                throw new ArgumentNullException(nameof(entityId));
            try
            {
                Logger.Info("Started Execution for AddDeal Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service: OfferEngine");

                if (request.dealOffers != null && request.dealOffers.Count > 0)
                {
                    UpdateLoanAmountRequest ApplicationLoanAmount = new UpdateLoanAmountRequest();
                    request.dealOffers[0].LenderId = GetOfferSource(request.Source);
                    request.dealOffers[0].CreatedBy = ExtractCurrentUser();
                    request.dealOffers[0].CreatedOn = new TimeBucket(TenantTime.Now);
                    ApplicationLoanAmount.LoanAmount = request.dealOffers[0].LoanAmount;
                    var application = await BusinessApplicationService.GetByApplicationNumber(entityId);
                    var RuleResult = await ProductRuleService.RunRule(entityType, entityId, application.ProductId, "SelectedDealOffer", request.dealOffers.FirstOrDefault());
                    if (RuleResult != null && RuleResult.Result == Result.Passed)
                    {
                        await BusinessApplicationService.UpdateLoanAmount(entityId, ApplicationLoanAmount);
                        await EventHubClient.Publish(new OfferComputed
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = request.dealOffers,
                            Request = request,
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                        return await OfferEngineRepository.AddDeal(entityId, request.dealOffers.FirstOrDefault());
                    }
                }
            }catch(Exception exception)
            {
                Logger.Error("Error While Processing AddDeal Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: OfferEngine" + "Exception" + exception.Message);
            }
            Logger.Info("Completed Execution for AddDeal Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service: OfferEngine");
            return null;
        }
        public async Task<IApplicationOffer> SaveOffer(string entityType, string entityId, IApplicationOffer offers)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (offers == null)
                throw new ArgumentNullException(nameof(offers));
            try
            {
                Logger.Info("Started Execution for SaveOffer Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service: OfferEngine");

                offers.Source = GetOfferSource(offers.Source);
                offers.CreatedBy = ExtractCurrentUser();
                offers.CreatedOn = new TimeBucket(TenantTime.Now);
                offers.EntityId = entityId;
                offers.EntityType = entityType;
                offers.Offers?.ForEach(i => i.DurationType = string.IsNullOrWhiteSpace(i.DurationType) ? OfferEngineConfigurations.DurationType : i.DurationType);
                return await OfferEngineRepository.AddUpdateOffer(entityId, offers);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing SaveOffer Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: OfferEngine" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<IDealOffer> GetDeal(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            try
            {
                return await OfferEngineRepository.GetDeal(entityId);
            }catch(Exception exception)
            {
                Logger.Error("Error While Processing GetDeal Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: OfferEngine" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<List<IDealOffer>> GetLenderDeal(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            try
            { 
            return await OfferEngineRepository.GetLenderDeals(entityId);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetLenderDeal Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: OfferEngine" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task<IApplicationOffer> AddLenderDeal(string entityType, string entityId, IDealOfferRequest request)
        {
            if (entityId == null)
                throw new ArgumentNullException(nameof(entityId));

            if (string.IsNullOrEmpty(request.Source))
                throw new ArgumentNullException(nameof(request.Source));
            try
            {
                Logger.Info("Started Execution for AddLenderDeal Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " Service: OfferEngine");

                if (request.dealOffers != null && request.dealOffers.Count > 0)
                {
                    string Source = GetOfferSource(request.Source);
                    string CurrentUser = ExtractCurrentUser();
                    foreach (var deal in request.dealOffers)
                    {
                        deal.CreatedBy = CurrentUser;
                        deal.CreatedOn = new TimeBucket(TenantTime.Now);
                        deal.LenderId = Source;
                    }

                    return await OfferEngineRepository.AddLenderDeal(entityId, request.dealOffers);
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddLenderDeal Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: OfferEngine" + "Exception" + exception.Message);
                throw;
            }
            return null;
        }

        public async Task<bool> DeleteOffersAndDeal(string entityType, string entityId)
        {
            if (entityId == null)
                throw new ArgumentNullException(nameof(entityId));
            try
            {
                return await OfferEngineRepository.DeleteOffers(entityId);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing DeleteOffersAndDeal Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Service: OfferEngine" + "Exception" + exception.Message);
                throw;
            }
        }

        #endregion Public Methods

        #region Private Methods
        public string ExtractCurrentUser()
        {
            var token = TokenHandler.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("User is not authorized");
            return username;
        }

        public string GetOfferSource(string RequestedSource)
        {
            string offerSource = OfferEngineConfigurations.DefaultSource;
            if (!string.IsNullOrEmpty(RequestedSource))
            {
                var source = LookupService.GetLookupEntry("offerSourceType", RequestedSource);
                if (source != null)
                {
                    offerSource = RequestedSource;
                }
            }
            return offerSource;
        }

        #endregion

    }
}