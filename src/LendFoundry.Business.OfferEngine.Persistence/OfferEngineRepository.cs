﻿using LendFoundry.Business.OfferEngine.Persistence;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Business.OfferEngine
{
    public class OfferEngineRepository : MongoRepository<IApplicationOffer, ApplicationOffer>, IOfferEngineRepository
    {
        #region Constructor

        static OfferEngineRepository()
        {
            // BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IApplicant, IApplicant>());
            BsonClassMap.RegisterClassMap<ApplicationOffer>(map =>
            {
                map.AutoMap();
                var type = typeof(ApplicationOffer);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
                // map.MapProperty(a => a.ApplicationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
            });

            BsonClassMap.RegisterClassMap<BusinessLoanOffer>(map =>
            {
                map.AutoMap();
                //map.MapProperty(p => p.RepaymentFrequency).SetSerializer(new EnumSerializer<LoanFrequency>(BsonType.String));
                var type = typeof(BusinessLoanOffer);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<OfferFee>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.FeeType).SetSerializer(new EnumSerializer<OfferFeeType>(BsonType.String));
                var type = typeof(OfferFee);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<DealOffer>(map =>
            {
                map.AutoMap();
                var type = typeof(DealOffer);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public OfferEngineRepository(LendFoundry.Tenant.Client.ITenantService tenantService, IMongoConfiguration configuration)
           : base(tenantService, configuration, "applicationoffer")
        {
            CreateIndexIfNotExists("applicationoffer_applicationofferId", Builders<IApplicationOffer>.IndexKeys.Ascending(i => i.EntityId));
        }

        #endregion Constructor

        #region Public Methods
        public async Task<IApplicationOffer> AddUpdateOffer(string applicationNumber, IApplicationOffer offers)
        {
            if (applicationNumber == null)
                throw new ArgumentNullException(nameof(applicationNumber));

            var filter = Builders<IApplicationOffer>.Filter.Where(application => application.EntityId == applicationNumber);
            var initialOffer = await Collection.Find(filter).FirstOrDefaultAsync();
            if (initialOffer == null)
            {
                Add(offers);
            }
            else
            {
                await Collection.UpdateOneAsync(Builders<IApplicationOffer>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                     a.EntityId == applicationNumber),
                Builders<IApplicationOffer>.Update.Set(a => a.Offers, offers.Offers));
            }
            return await Task.Run(() => Query.Where(i => i.EntityId == applicationNumber).FirstOrDefault());
        }

        public async Task<IApplicationOffer> GetApplicationOffers(string applicationNumber)
        {
            if (applicationNumber == null)
                throw new ArgumentNullException(nameof(applicationNumber));
            return await Task.Run(() => Query.Where(i => i.EntityId == applicationNumber).FirstOrDefault());
        }

        private async Task AddDealHistory(string applicationNumber)
        {
            if (applicationNumber == null)
                throw new ArgumentNullException(nameof(applicationNumber));

            var offers = await GetApplicationOffers(applicationNumber);

            if (offers == null)
                throw new NotFoundException($"Offer for {applicationNumber} not found");
            if(offers.DealHistory==null)
            {
                offers.DealHistory = new List<IDealOffer>();
            }
            if (offers.DealOffer != null)
            {
                offers.DealHistory.Add(offers.DealOffer);
            }

            await Collection.UpdateOneAsync(Builders<IApplicationOffer>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                        a.EntityId == applicationNumber),
               Builders<IApplicationOffer>.Update.Set(a => a.DealHistory, offers.DealHistory));
        }

        public async Task<IApplicationOffer> AddDeal(string applicationNumber, IDealOffer request)
        {
            if (applicationNumber == null)
                throw new ArgumentNullException(nameof(applicationNumber));

            await AddDealHistory(applicationNumber);

            await Collection.UpdateOneAsync(Builders<IApplicationOffer>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.EntityId == applicationNumber),
            Builders<IApplicationOffer>.Update.Set(a => a.DealOffer, request));
            
            return await Task.Run(() => Query.Where(i => i.EntityId == applicationNumber).FirstOrDefault());
        }

        public async Task<IApplicationOffer> AddLenderDeal(string applicationNumber, List<IDealOffer> request)
        {
            if (applicationNumber == null)
                throw new ArgumentNullException(nameof(applicationNumber));

            await Collection.UpdateOneAsync(Builders<IApplicationOffer>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.EntityId == applicationNumber),
            Builders<IApplicationOffer>.Update.Set(a => a.LenderDeals, request));
            return await Task.Run(() => Query.Where(i => i.EntityId == applicationNumber).FirstOrDefault());
        }

        public async Task<IDealOffer> GetDeal(string applicationNumber)
        {
            if (applicationNumber == null)
                throw new ArgumentNullException(nameof(applicationNumber));
            var deal = await Task.Run(() => Query.Where(i => i.EntityId == applicationNumber).Select(i => i.DealOffer));
            return deal?.FirstOrDefault();
        }

        public async Task<List<IDealOffer>> GetLenderDeals(string applicationNumber)
        {
            if (applicationNumber == null)
                throw new ArgumentNullException(nameof(applicationNumber));
            var lenderDeals = await Task.Run(() => Query.Where(i => i.EntityId == applicationNumber).SelectMany(i => i.LenderDeals));
            return lenderDeals?.ToList();
        }

        public async Task<bool> DeleteOffers(string applicationNumber)
        {
            var filter = Builders<IApplicationOffer>.Filter.Where(application => application.EntityId == applicationNumber);
            var offers = await Collection.Find(filter).FirstOrDefaultAsync();
            if (offers != null)
            {
                Remove(offers);
            }
            return true;
        }
    }

    #endregion Public Methods
}