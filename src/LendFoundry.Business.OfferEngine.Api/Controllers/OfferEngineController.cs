﻿using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Business.OfferEngine.Api.Controllers
{
    /// <summary>
    /// OfferEngineController class 
    /// </summary>
    [Route("/")]
    public class OfferEngineController : ExtendedController
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service"></param>
        public OfferEngineController(IOfferEngineService service)
        {
            OfferEngineService = service;
        }

        #endregion Constructors

        #region Private Properties
               
        private IOfferEngineService OfferEngineService { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// GetApplicationOffers
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entitytype}/{entityid}/offerengine/getapplicationoffers")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetApplicationOffers(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.GetApplicationOffers(entityType, entityId))));
        }

        /// <summary>
        /// AddDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/offerengine/deal")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddDeal(string entityType, string entityId, [FromBody]DealOfferRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.AddDeal(entityType, entityId, request))));
        }

        /// <summary>
        /// AddLenderDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/offerengine/lender/deal")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddLenderDeal(string entityType, string entityId, [FromBody]DealOfferRequest request)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.AddLenderDeal(entityType, entityId, request))));
        }

        /// <summary>
        /// SaveOffers
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="offers"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/offerengine/saveoffer")]
        [ProducesResponseType(typeof(IApplicationOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> SaveOffers(string entityType, string entityId, [FromBody]ApplicationOffer offers)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));

            return await ExecuteAsync(async () => Ok(await (OfferEngineService.SaveOffer(entityType, entityId, offers))));
        }
        
        /// <summary>
        /// GetDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entitytype}/{entityid}/deal")]
        [ProducesResponseType(typeof(IDealOffer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetDeal(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await (OfferEngineService.GetDeal(entityType, entityId))));
        }

        /// <summary>
        /// GetLenderDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("/{entitytype}/{entityid}/lender/deal")]
        [ProducesResponseType(typeof(IDealOffer[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLenderDeal(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await (OfferEngineService.GetLenderDeal(entityType, entityId))));
        }

        /// <summary>
        /// DeleteOffersAndDeal
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpDelete("/{entitytype}/{entityid}/delete/offers")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DeleteOffersAndDeal(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await (OfferEngineService.DeleteOffersAndDeal(entityType, entityId))));
        }

        #endregion Public Methods
    }
}